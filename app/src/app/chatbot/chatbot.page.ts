import { Component, OnInit } from '@angular/core';
import { ChatService } from '../services/chat/chat.service';
import { IChat } from '../models/chat.model';

@Component({
  selector: 'app-chatbot',
  templateUrl: 'chatbot.page.html',
  styleUrls: ['chatbot.page.scss'],
})
export class ChatbotPage implements OnInit {
  private chats: IChat[] = [];
  private message: string;

  constructor(private _chat: ChatService) {}

  ngOnInit() {
    this._chat.getChannel().bind('chat', data => {
      if (data.type !== 'bot') {
        data.isMe = true;
        this.chats.push(data);
      } else {
        setTimeout(() => {
          this.chats.push(data);
        }, 500);
      }
    });
  }

  sendMessage() {
    this._chat.sendMessage(this.message).subscribe(
      resp => {
        this.message = '';
      },
      err => {}
    );
  }
}
