import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

declare const Pusher: any;

@Injectable()
export class PusherService {
  private _pusher: any;

  constructor(public http: HttpClient) {
    this._pusher = new Pusher('dffe007e1eda0c751114', {
      cluster: 'eu',
      forceTLS: true,
    });
  }

  getPusher() {
    return this._pusher;
  }
}
