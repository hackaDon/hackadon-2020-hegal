import { PusherService } from '../pusher/pusher.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ChatService {
  private url = 'http://localhost:5000';
  private channel: any;

  constructor(public http: HttpClient, private pusher: PusherService) {
    this.channel = this.pusher.getPusher().subscribe('chat-bot');
  }

  sendMessage(message: string): Observable<any> {
    const param = {
      type: 'human',
      message,
    };
    return this.http.post(`${this.url}/message`, param);
  }

  getChannel() {
    return this.channel;
  }
}
