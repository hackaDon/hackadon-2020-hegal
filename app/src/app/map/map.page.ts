import { Component } from '@angular/core';
import { ModalController, Platform } from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  Environment,
} from '@ionic-native/google-maps';

import markers from './markers';

import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-map',
  templateUrl: 'map.page.html',
  styleUrls: ['map.page.scss'],
})
export class MapPage {
  private map: GoogleMap;
  private initalLat = 48.868698;
  private initalLng = 2.34115;

  constructor(
    private modalCtrl: ModalController,
    private platform: Platform,
    private geolocation: Geolocation
  ) {
    this.platform.ready().then(() => {
      this.getCurrentPositionAndLoadMapAndMarker();
    });
  }

  async getCurrentPositionAndLoadMapAndMarker() {
    await this.getCurrentPosition();
    await this.loadMap();
    this.placeMarkers();
  }

  async getCurrentPosition() {
    this.geolocation
      .getCurrentPosition({
        timeout: 10000,
        enableHighAccuracy: true,
        maximumAge: 3600,
      })
      .then(resp => {
        this.initalLat = resp.coords.latitude;
        this.initalLng = resp.coords.longitude;
        return { lat: this.initalLat, lng: this.initalLng };
      })
      .catch(error => {
        console.log('Error getting location', error);
        return null;
      });
  }

  async loadMap() {
    Environment.setEnv({
      API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyAbnIdeAJdu6TWycXwi_PkUjv3TaKt_8EA',
      API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyAbnIdeAJdu6TWycXwi_PkUjv3TaKt_8EA',
    });

    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: {
          lat: this.initalLat,
          lng: this.initalLng,
        },
        zoom: 18,
        tilt: 30,
      },
      controls: {
        myLocationButton: true,
        myLocation: true,
      },
    });

    this.map.addMarkerSync({
      title: 'Vous',
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: this.initalLat,
        lng: this.initalLng,
      },
    });
  }

  placeMarkers() {
    markers.forEach(mark => {
      let marker: Marker = this.map.addMarkerSync({
        title: mark.fridge
          ? `${mark.label}<br/>${mark.adress}`
          : `${mark.label}<br/>${mark.adress}<br/><a target="_blank" href="${mark.web}">Site web</a>`,
        icon: mark.fridge
          ? {
              url: 'assets/fridge.png',
              size: {
                width: 20,
                height: 30,
              },
            }
          : {
              url: 'assets/founds.png',
              size: {
                width: 20,
                height: 30,
              },
            },
        animation: 'DROP',
        position: {
          lat: mark.lat,
          lng: mark.lng,
        },
      });

      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        console.log('MARKET CLICKED - PING API FOR STAT');
      });
    });
  }

  dismissMap() {
    this.modalCtrl.dismiss({
      dismissed: true,
    });
  }
}
