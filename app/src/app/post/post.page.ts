import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-post',
  templateUrl: 'post.page.html',
  styleUrls: ['post.page.scss'],
})
export class PostPage implements OnDestroy, OnInit {
  private subscription;
  private newsId = null;
  private postContent = {
    title: 'faire un don',
    body: '',
  };

  constructor(
    private route: ActivatedRoute,
    private loadingController: LoadingController
  ) {
    this.subscription = this.route.params.subscribe(params => {
      this.newsId = params['newsId'];
    });
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Je cherche...',
    });
    await loading.present();

    //TODO: HTTP CALL FOR POST DEFAILT
    //TODO: ASSIGN POSTCONTENT TO THE CONTENT DATA FROM API
    //TODO: DISMISS LOADING AFTER HTTP CALL
    //TODO: NAVIGATE TO /NEWS IF HTTP ERROR

    await loading.dismiss();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
