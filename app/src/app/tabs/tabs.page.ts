import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NavController } from '@ionic/angular';

import { MapPage } from '../map/map.page';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss'],
})
export class TabsPage {
  constructor(
    private modalController: ModalController,
    private navCtrl: NavController
  ) {}

  async presentMap() {
    const modal = await this.modalController.create({
      component: MapPage,
    });
    return await modal.present();
  }

  goHome() {
    this.navCtrl.navigateRoot(['tabs', 'home']);
  }
}
