import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-news',
  templateUrl: 'news.page.html',
  styleUrls: ['news.page.scss'],
})
export class NewsPage {
  constructor(private navCtrl: NavController) {}

  refreshNews(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

  openPost(id: String) {
    this.navCtrl.navigateForward(['tabs', 'news', id]);
  }
}
